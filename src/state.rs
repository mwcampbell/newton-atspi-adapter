// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

// Derived from smithay-clipboard.
// Copyright (c) 2018 Lucas Timmins & Victor Berger
// Licensed under the MIT license (found in the LICENSE-MIT file).

use accesskit::{ActionHandler, ActionRequest, TreeUpdate};
use accesskit_consumer::Tree;
use accesskit_unix::Adapter;
use sctk::{
    data_device_manager::{ReadPipe, WritePipe},
    delegate_registry, delegate_seat,
    reexports::{
        calloop::{LoopHandle, PostAction},
        client::{
            globals::GlobalList,
            protocol::{wl_keyboard::WlKeyboard, wl_seat::WlSeat},
            Connection, Dispatch, Proxy, QueueHandle,
        },
    },
    registry::{ProvidesRegistryState, RegistryHandler, RegistryState},
    registry_handlers,
    seat::{Capability, SeatHandler, SeatState},
};
use std::{
    collections::{HashMap, HashSet},
    io::{ErrorKind, Read, Write},
    os::unix::io::{AsFd, AsRawFd, OwnedFd, RawFd},
    sync::{Arc, Mutex, OnceLock, Weak},
};
use wayland_protocols::wp::a11y_consumer::v1::client::{
    wp_a11y_consumer_surface_v1::{Event as SurfaceEvent, WpA11yConsumerSurfaceV1},
    wp_a11y_consumer_updates_v1::{Event as UpdatesEvent, WpA11yConsumerUpdatesV1},
};

#[derive(Clone, Copy)]
struct SurfaceData {
    name: u32,
}

struct AdapterState {
    adapter: Adapter,
    tree: Weak<Mutex<Tree>>,
}

// TODO: real action handler
struct MyActionHandler;

impl ActionHandler for MyActionHandler {
    fn do_action(&mut self, request: ActionRequest) {}
}

struct SurfaceState {
    surface: WpA11yConsumerSurfaceV1,
    focused_keyboards: HashSet<WlKeyboard>,
    updates: Option<WpA11yConsumerUpdatesV1>,
    adapter_state: OnceLock<AdapterState>,
}

impl SurfaceState {
    fn has_focus(&self) -> bool {
        !self.focused_keyboards.is_empty()
    }

    fn ensure_updates(&mut self, data: &SurfaceData, qh: &QueueHandle<State>) {
        if self.updates.is_none() {
            self.updates = Some(self.surface.get_updates(qh, *data));
        }
    }

    fn handle_update(&self, update: TreeUpdate) {
        if let Some(adapter_state) = self.adapter_state.get() {
            if let Some(tree) = adapter_state.tree.upgrade() {
                tree.lock().unwrap().update(update.clone());
            }
            adapter_state.adapter.update_if_active(|| update);
        } else {
            let tree = Arc::new(Mutex::new(Tree::new(update, true)));
            let weak_tree = Arc::downgrade(&tree);
            let adapter = Adapter::new(
                move || tree.lock().unwrap().state().serialize(),
                Box::new(MyActionHandler {}),
            );
            if self.has_focus() {
                adapter.update_window_focus_state(true);
            }
            let _ = self.adapter_state.set(AdapterState {
                adapter,
                tree: weak_tree,
            });
        }
    }
}

impl Drop for SurfaceState {
    fn drop(&mut self) {
        self.surface.destroy();
        if let Some(updates) = &self.updates {
            updates.destroy();
        }
    }
}

pub(crate) struct State {
    registry_state: RegistryState,
    seat_state: SeatState,
    loop_handle: LoopHandle<'static, Self>,
    surfaces: HashMap<u32, SurfaceState>,
    keyboards: HashMap<WlSeat, WlKeyboard>,
}

impl State {
    pub(crate) fn new(
        globals: &GlobalList,
        qh: &QueueHandle<State>,
        loop_handle: LoopHandle<'static, Self>,
    ) -> Self {
        let mut state = Self {
            registry_state: RegistryState::new(globals),
            seat_state: SeatState::new(globals, qh),
            loop_handle,
            surfaces: HashMap::new(),
            keyboards: HashMap::new(),
        };

        globals.contents().with_list(|globals| {
            for global in globals {
                if global.interface != WpA11yConsumerSurfaceV1::interface().name {
                    continue;
                }
                state.handle_new_surface(qh, global.name, global.version);
            }
        });

        state
    }

    fn handle_new_surface(&mut self, qh: &QueueHandle<Self>, name: u32, version: u32) {
        let data = SurfaceData { name };
        let surface = self
            .registry()
            .bind_specific::<WpA11yConsumerSurfaceV1, _, _>(qh, name, 1..=version, data)
            .unwrap();
        let state = SurfaceState {
            surface,
            focused_keyboards: HashSet::new(),
            updates: None,
            adapter_state: OnceLock::new(),
        };
        self.surfaces.insert(name, state);
    }

    fn remove_keyboard(&mut self, seat: WlSeat) {
        if let Some(keyboard) = self.keyboards.remove(&seat) {
            keyboard.release();
        }
    }

    fn handle_update(&mut self, surface_data: SurfaceData, fd: OwnedFd) {
        let read_pipe = ReadPipe::from(fd);
        unsafe {
            if set_non_blocking(read_pipe.as_raw_fd()).is_err() {
                return;
            }
        }
        let mut reader_buffer = [0; 4096];
        let mut content = Vec::new();
        let _ = self
            .loop_handle
            .insert_source(read_pipe, move |_, file, state| {
                let file = unsafe { file.get_mut() };
                loop {
                    match file.read(&mut reader_buffer) {
                        Ok(0) => {
                            let update = match serde_json::from_slice::<TreeUpdate>(&content) {
                                Ok(update) => update,
                                Err(_) => {
                                    break PostAction::Remove;
                                }
                            };
                            let surface_state = match state.surfaces.get(&surface_data.name) {
                                Some(state) => state,
                                None => {
                                    break PostAction::Remove;
                                }
                            };
                            surface_state.handle_update(update);
                            break PostAction::Remove;
                        }
                        Ok(n) => content.extend_from_slice(&reader_buffer[..n]),
                        Err(err) if err.kind() == ErrorKind::WouldBlock => {
                            break PostAction::Continue
                        }
                        Err(_) => {
                            break PostAction::Remove;
                        }
                    };
                }
            });
    }
}

impl ProvidesRegistryState for State {
    registry_handlers![State, SeatState];

    fn registry(&mut self) -> &mut RegistryState {
        &mut self.registry_state
    }
}

impl RegistryHandler<State> for State {
    fn new_global(
        state: &mut Self,
        _: &Connection,
        qh: &QueueHandle<Self>,
        name: u32,
        interface: &str,
        version: u32,
    ) {
        if interface != WpA11yConsumerSurfaceV1::interface().name {
            return;
        }
        state.handle_new_surface(qh, name, version);
    }

    fn remove_global(
        state: &mut Self,
        _: &Connection,
        _: &QueueHandle<Self>,
        name: u32,
        interface: &str,
    ) {
        if interface != "wp_a11y_consumer_surface_v1" {
            return;
        }
        state.surfaces.remove(&name);
    }
}

impl SeatHandler for State {
    fn seat_state(&mut self) -> &mut SeatState {
        &mut self.seat_state
    }

    fn new_seat(&mut self, _: &Connection, _: &QueueHandle<Self>, _: WlSeat) {}

    fn new_capability(
        &mut self,
        _: &Connection,
        qh: &QueueHandle<Self>,
        seat: WlSeat,
        capability: Capability,
    ) {
        if let Capability::Keyboard = capability {
            let keyboard = seat.get_keyboard(qh, ());
            self.keyboards.insert(seat, keyboard);
        }
    }

    fn remove_capability(
        &mut self,
        _: &Connection,
        _: &QueueHandle<Self>,
        seat: WlSeat,
        capability: Capability,
    ) {
        if let Capability::Keyboard = capability {
            self.remove_keyboard(seat);
        }
    }

    fn remove_seat(&mut self, _: &Connection, _: &QueueHandle<Self>, seat: WlSeat) {
        self.remove_keyboard(seat);
    }
}

impl Dispatch<WpA11yConsumerSurfaceV1, SurfaceData> for State {
    fn event(
        state: &mut Self,
        _: &WpA11yConsumerSurfaceV1,
        event: SurfaceEvent,
        data: &SurfaceData,
        _: &Connection,
        qh: &QueueHandle<State>,
    ) {
        let surface_state = match state.surfaces.get_mut(&data.name) {
            Some(state) => state,
            None => return,
        };
        match event {
            SurfaceEvent::KeyboardEnter { keyboard } => {
                let had_focus = surface_state.has_focus();
                surface_state.focused_keyboards.insert(keyboard);
                surface_state.ensure_updates(data, qh);
                if !had_focus {
                    if let Some(adapter_state) = surface_state.adapter_state.get() {
                        adapter_state.adapter.update_window_focus_state(true);
                    }
                }
            }
            SurfaceEvent::KeyboardLeave { keyboard } => {
                surface_state.focused_keyboards.remove(&keyboard);
                if !surface_state.has_focus() {
                    if let Some(adapter_state) = surface_state.adapter_state.get() {
                        adapter_state.adapter.update_window_focus_state(false);
                    }
                }
            }
            _ => (),
        }
    }
}

impl Dispatch<WpA11yConsumerUpdatesV1, SurfaceData> for State {
    fn event(
        state: &mut Self,
        _: &WpA11yConsumerUpdatesV1,
        event: UpdatesEvent,
        data: &SurfaceData,
        _: &Connection,
        _: &QueueHandle<State>,
    ) {
        if let UpdatesEvent::Update { fd } = event {
            state.handle_update(*data, fd);
        }
    }
}

impl Dispatch<WlKeyboard, ()> for State {
    fn event(
        _: &mut Self,
        _: &WlKeyboard,
        _: <WlKeyboard as Proxy>::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<State>,
    ) {
    }
}

delegate_registry!(State);
delegate_seat!(State);

unsafe fn set_non_blocking(raw_fd: RawFd) -> std::io::Result<()> {
    let flags = libc::fcntl(raw_fd, libc::F_GETFL);

    if flags < 0 {
        return Err(std::io::Error::last_os_error());
    }

    let result = libc::fcntl(raw_fd, libc::F_SETFL, flags | libc::O_NONBLOCK);
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }

    Ok(())
}
