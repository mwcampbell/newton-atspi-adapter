# AT-SPI adapter for Wayland-native accessibility

This program receives accessibility information via the Wayland a11y-consumer-v1 protocol and uses that information to implement AT-SPI. In other words, it's a translator between Wayland-native accessibility and AT-SPI.

It's based on the current AccessKit AT-SPI adapter, and inherits that adapter's limitations, which currently include lack of text support.

This program doesn't yet support action requests. It also isn't yet optimized to be as lazy as possible.

## Running the end-to-end prototype

To run an end-to-end prototype of the Wayland-native accessibility stack, first set up all of the components:

1. [Install Rust]https://rustup.rs/).

2. To compile a demo application, check out the `accesskit-unix2-prototype` branch of [my egui fork](https://github.com/mwcampbell/egui) and build the `hello_world` sample. Run the following from the root of the egui checkout:

    ```bash
    cargo build -p hello_world
    ```

3. Build and install the `wayland-protocols` package from the `accessibility` branch of [my fork of wayland-protocols](https://gitlab.freedesktop.org/mwcampbell/wayland-protocols), using the standard procedure for building and installing this package with Meson.

4. Build and install Mutter from the `wayland-native-a11y` branch of [my fork of Mutter](https://gitlab.gnome.org/mwcampbell/mutter), again using the standard procedure for building and installing this package with Meson.

5. Build this adapter. Run the following from the root of the checkout for this repository:

    ```bash
    cargo build
    ```

Then, to run the prototype:

1. Start Mutter or GNOME Shell as appropriate for your environment.

2. Start Orca or another assistive technology that supports AT-SPI.

3. Start `newton-atspi-adapter` in a terminal within the Mutter or GNOME Shell session. In the root for the checkout of this repository, run:

    ```bash
    ./target/debug/newton-atspi-adapter
    ```

4. Run the egui `hello_world` example from another terminal within the Mutter or GNOME Shell session. Run the following in the root of the egui checkout:

    ```bash
    ./target/debug/hello_world
    ```

Orca won't speak anything when the egui `hello_world` initially receives focus. But once you focus a control in the window, e.g. by pressing Tab, Orca will start speaking.
